// 1) Baja el archivo grades.json y en la terminal ejecuta el siguiente comando: $ mongoimport -d students -c grades < grades.json

/*

    -- Terminal --

        $ cd <directorio_con_archivo_json>
        $ docker cp grades.json <nombre_del_contenedor>:/tmp/grades.json
        $ docker exec <nombre_de_contendor> mongoimport -d students -c grades --file /tmp/grades.json

    -- Respuesta --
    
        - 2022-11-06T20:47:10.694+0000	connected to: mongodb://localhost/
        - 2022-11-06T20:47:10.712+0000	800 document(s) imported successfully. 0 document(s) failed to import.

*/

// 2) El conjunto de datos contiene 4 calificaciones de n estudiantes. Confirma que se importo correctamente la 
//    colección con los siguientes comandos en la terminal de mongo: >use students; >db.grades.count() 
//    ¿Cuántos registros arrojo el comando count?

/*

    -- Terminal --

        $ docker exec -ti <nombre_del_contenedor> mongosh
            > use students;
            > db.grades.count()

    -- Respuesta --
    
        - 800

*/

// 3) Encuentra todas las calificaciones del estudiante con el id numero 4.

/*

    -- Terminal --

        > db.grades.find({"student_id": 4}, {"_id": 0, "student_id": 1, "score": 1})

    -- Respuesta --
    
        - [
        -    { student_id: 4, score: 87.89071881934647 },
        -    { student_id: 4, score: 27.29006335059361 },
        -    { student_id: 4, score: 5.244452510818443 },
        -    { student_id: 4, score: 28.656451042441 }
        - ]

*/

// 4) ¿Cuántos registros hay de tipo exam?

/*

    -- Terminal --

        > db.grades.count({"type": "exam"})

    -- Respuesta --
    
        - 200

*/

// 5) ¿Cuántos registros hay de tipo homework?

/*

    -- Terminal --

        > db.grades.count({"type": "homework"})

    -- Respuesta --
    
        - 400

*/

// 6) ¿Cuántos registros hay de tipo quiz?

/*

    -- Terminal --

        > db.grades.count({"type": "quiz"})

    -- Respuesta --
    
        - 200

*/

// 7) Elimina todas las calificaciones del estudiante con el id numero 3

/*

    -- Terminal --

        > db.grades.updateMany({"student_id": 3}, {$unset: {"score": ""}})

    -- Respuesta --
    
        - {
        -     acknowledged: true,
        -     insertedId: null,
        -     matchedCount: 4,
        -     modifiedCount: 4,
        -     upsertedCount: 0
        - }
    
    -- Terminal --

        > db.grades.find({"student_id": 3})

    -- Respuesta --

    - [
    -     {
    -         _id: ObjectId("50906d7fa3c412bb040eb584"),
    -         student_id: 3,
    -         type: 'quiz'
    -     },
    -     {
    -         _id: ObjectId("50906d7fa3c412bb040eb585"),
    -         student_id: 3,
    -         type: 'homework'
    -     },
    -     {
    -         _id: ObjectId("50906d7fa3c412bb040eb583"),
    -         student_id: 3,
    -         type: 'exam'
    -     },
    -     {
    -         _id: ObjectId("50906d7fa3c412bb040eb586"),
    -         student_id: 3,
    -         type: 'homework'
    -     }
    - ]

*/

// 8) ¿Qué estudiantes obtuvieron 75.29561445722392 en una tarea ?

/*

    -- Terminal --

        > db.grades.find({$and: [{"score": 75.29561445722392}, {"type": "homework"}]}, {"_id": 0, "student_id": 1})

    -- Respuesta --
    
        - [ { student_id: 9 } ]

*/

// 9) Actualiza las calificaciones del registro con el uuid 50906d7fa3c412bb040eb591 por 100

/*

    -- Terminal --

        > db.grades.updateMany({"_id": ObjectId("50906d7fa3c412bb040eb591")}, {$set: {"score": 100}})

    -- Respuesta --
    
        - {
        -     acknowledged: true,
        -     insertedId: null,
        -     matchedCount: 1,
        -     modifiedCount: 1,
        -     upsertedCount: 0
        - }

    -- Terminal --

        > db.grades.find({"_id": ObjectId("50906d7fa3c412bb040eb591")})

    -- Respuesta --
    
        - [
        -     {
        -         _id: ObjectId("50906d7fa3c412bb040eb591"),
        -         student_id: 6,
        -         type: 'homework',
        -         score: 100
        -     }
        - ]

*/

// 10) A qué estudiante pertenece esta calificación.

/*

    -- Terminal --

        > db.grades.find({"_id": ObjectId("50906d7fa3c412bb040eb591")}, {"_id": 0, "student_id": 1})

    -- Respuesta --
    
        - [ { student_id: 6 } ]

*/